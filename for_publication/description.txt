--- EN ---
Protects the portal from reading the tentaclea book, planting a spider cocoon, a fossils, a lureplant next to it, as well as destroying the skeletons of players.
In the settings, you can change the radiuses or disable any protection. By default, the protection does not apply to admins, but you can enable it in the settings.
Recommended for public servers.

--- RU ---
�������� ������ �� ������ ������������ �����, ������� ����� � ��� �������� ������, ������������ � �������������, � ��� �� ���������� ������� �������.
� ���������� ����� �������� ������� ��� ��������� �����-���� ������. �� ��������� ������ �� ���������������� �� �������, �� ����� � �������� � ����������.
������������� ��� ��������� ��������.
